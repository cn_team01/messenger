#pragma once

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>

#include <string>

using namespace std;


const int MAX_CONNECTIONS = 20;
const int MAX_RECV = 4096;

class Socket
{
	public:
		Socket(const int port);
		Socket() {}
		virtual ~Socket();
		
		void accept(Socket& new_socket) const;

		const Socket& operator>>(string& mesg) const;
		const Socket& operator<<(const string& mesg) const;

	private:
		int socket_fd;

		void create();
		void bind(const int port);
		void listen() const;
};
