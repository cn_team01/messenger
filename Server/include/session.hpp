#pragma once

#include "socket.hpp"
#include "user.hpp"				// User
#include "rapidxml.hpp"

#include <mysql_connection.h>	// Connection

using namespace sql;
using namespace rapidxml;


class Session
{
	public:
		bool create(Socket& socket, Connection* db_con);
		void destroy(Socket& socket);
		void listen_to_client(Socket& socket);
		void request_handler(string& message, Socket& socket);

	private:
		User curr_user;

		void forward_message(Socket& socket, xml_node<>* mesg, string mesg_type, string raw_mesg);
		void send_user_list(Socket& socket);
		bool login(Socket& socket, Connection* db_con);
		void logout(Socket& socket);
};
