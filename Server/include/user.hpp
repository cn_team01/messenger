#pragma once

#include "socket.hpp"

#include <string>
#include <mysql_connection.h>

using namespace std;
using namespace sql;


class User
{
	public:
		User(string& u_id, string& u_pass): id(u_id), password(u_pass) {}
		User() {}
		string get_id();
		string get_password();
		bool is_logged_in();
		Socket get_socket();
		void create(Connection* db_con);

	private:
		string id;
		string password;
		bool login;
};
