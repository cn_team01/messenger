#pragma once

#include "session.hpp"			// Session

#include <string>
#include <mysql_connection.h>	// Connection

using namespace std;
using namespace sql;


class Server
{
	public:
		Server();
		void connection_handler(Socket& socket);

	private:
		Connection* db_con;
		Session session;

		void db_connect();
		string read_password();
};
