#include "server.hpp"
#include "socket.hpp"

#include <unistd.h>
#include <termios.h>

#include <iostream>
#include <thread>
#include <string>
#include <stdexcept>

#include <mysql_connection.h>
#include <cppconn/exception.h>
#include <cppconn/driver.h>

using namespace sql;
using namespace std;


Server::Server()
{
	db_connect();
}

void Server::db_connect()
{
	try
	{
		cout << "Enter password for mysql root: ";
		string mysql_root_password = read_password();
		db_con = get_driver_instance()->connect("tcp://127.0.0.1:3306", "root", 
												mysql_root_password);
		db_con->setSchema("Messenger");
		cerr << "\nConnected to database.\n";
		return;
	}
	catch (SQLException &e) 
	{
		cerr << '\n' << "# SQL exception: " << e.what() << '\n';
		cerr << "  (MySQL error code: " << e.getErrorCode();
		cerr << ", SQLState: " << e.getSQLState() << ")" << "\n\n";
		exit(EXIT_FAILURE);
	}
}

string Server::read_password()
{
	termios oldt, newt;
	if (tcgetattr(STDIN_FILENO, &oldt) == -1)
		throw runtime_error("tcgetattr: " + string(strerror(errno)));
	
	newt = oldt;
	newt.c_lflag &= ~ECHO;
	/* Mask console input */
	if (tcsetattr(STDIN_FILENO, TCSANOW, &newt) == -1)
		throw runtime_error("tcsetattr: " + string(strerror(errno)));
	/* Read password for MySQL root */
	string password;
	getline(cin, password);
	/* Reset console input */
	if (tcsetattr(STDIN_FILENO, TCSANOW, &oldt) == -1)
		throw runtime_error("tcsetattr: " + string(strerror(errno)));

	return password;
}

void Server::connection_handler(Socket& socket)
{
	try
	{
		bool login_success = session.create(socket, db_con);
		while (not login_success)
			login_success = session.create(socket, db_con);
			
		string message;
		while (true)
		{
			socket >> message;
			cerr << message << endl;

			thread(&Session::request_handler, session, ref(message), ref(socket)).detach();
		}
		
		return;
	}
	catch (SQLException &e) 
	{
		cerr << "# SQL exception: " << e.what() << '\n';
		cerr << "  (MySQL error code: " << e.getErrorCode();
		cerr << ", SQLState: " << e.getSQLState() << ")" << "\n";
	}
	catch (runtime_error& e)
	{
		cerr << "Runtime error exception: " << e.what() << '\n';
	}
}
