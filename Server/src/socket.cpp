#include "socket.hpp"

#include <cstdio>
#include <cstring>
#include <cerrno>

#include <iostream>
#include <stdexcept>

using namespace std;


Socket::Socket(const int port): socket_fd(-1)
{
	create();
	bind(port);
	listen();

	cerr << "listening on port " << port << "..\n";
}

Socket::~Socket()
{
	if (socket_fd > 0)
		close(socket_fd);
}

void Socket::create()
{
	socket_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (socket_fd == -1)
		throw runtime_error("socket: " + string(strerror(errno)));
	
	int on = 1;
	if (setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, (const char*)&on, sizeof(on)) == -1)
		throw runtime_error("setsockopt: " + string(strerror(errno)));

	return;
}

void Socket::bind(const int port)
{
	sockaddr_in svr_addr;

	memset(&svr_addr, 0, sizeof(svr_addr));
	svr_addr.sin_family = AF_INET;
	svr_addr.sin_addr.s_addr = INADDR_ANY;
	svr_addr.sin_port = htons(port);

	if (::bind(socket_fd, (struct sockaddr*)&svr_addr, sizeof(svr_addr)) == -1)
		throw runtime_error("bind: " + string(strerror(errno)));

	return;
}

void Socket::listen() const
{
	if (::listen(socket_fd, MAX_CONNECTIONS) == -1)
		throw runtime_error("listen: " + string(strerror(errno)));
	
	return;
}

void Socket::accept(Socket& new_socket) const
{
	sockaddr_in cli_addr;
	socklen_t cli_addr_length = sizeof(cli_addr_length);

	new_socket.socket_fd = ::accept(socket_fd, (struct sockaddr*)&cli_addr, &cli_addr_length);
	if (new_socket.socket_fd == -1)
		throw runtime_error("accept: " + string(strerror(errno)));
	
	cerr << "Client connected.\n";
	return;
}

const Socket& Socket::operator>>(string& mesg) const
{
	char buf[MAX_RECV + 1];
	memset(buf, 0, MAX_RECV + 1);

	if (read(socket_fd, buf, MAX_RECV) == -1)
		throw runtime_error("recv: " + string(strerror(errno)));
	mesg = buf;

	return *this;
}

const Socket& Socket::operator<<(const string& mesg) const
{
	if (write(socket_fd, mesg.c_str(), mesg.size()) == -1)
		throw runtime_error("send: " + string(strerror(errno)));

	return *this;
}
