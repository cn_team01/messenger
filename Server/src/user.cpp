#include "user.hpp"
#include <cstdlib>

#include <iostream>
#include <string>

#include <mysql_connection.h>
#include <cppconn/statement.h>
#include <cppconn/exception.h>

using namespace std;
using namespace sql;


string User::get_id()
{
	return id;
}

string User::get_password()
{
	return password;
}

bool User::is_logged_in()
{
	return login;
}

void User::create(Connection* db_con)
{
	string query;
	query = "INSERT INTO Users (ID, Password) VALUES (";
	query += "\'" + id + "\'";
	query += ", ";
	query += "PASSWORD(\'" + password + "\')";
	query += ")";

	db_con->createStatement()->execute(query);
	cerr << "User " << id << " created.\n";

	return;
}
