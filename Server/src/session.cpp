#include "session.hpp"
#include "socket.hpp"
#include "rapidxml.hpp"
#include "global.hpp"

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <unistd.h>

#include <iostream>
#include <string>
#include <thread>
#include <stdexcept>

#include <mysql_connection.h>
#include <cppconn/statement.h>
#include <cppconn/resultset.h>
#include <cppconn/exception.h>

#define MSG_SIZE 4096
#define handle_error(msg) \
	do { perror(msg); exit(EXIT_FAILURE); } while (0)

using namespace std;
using namespace rapidxml;
using namespace sql;


bool Session::create(Socket& socket, Connection* db_con)
{
	string message;
	socket >> message;

	xml_document<> doc;
	doc.parse<0>(&message[0]);
	xml_node<>* auth = doc.first_node("auth");
	string type		= auth->first_attribute("type")->value();
	string ID		= auth->first_node("id")->value();
	string Password	= auth->first_node("pass")->value();
	curr_user = User(ID, Password);

	if (type == "signup")
	{
		curr_user.create(db_con);
		socket << "1\n";
	}
	if (not login(socket, db_con))
		return false;

	socket_map[curr_user.get_id()] = socket;
	return true;
}

void Session::request_handler(string& message, Socket& socket)
{
	string raw_mesg = message;
	xml_document<> doc;
	doc.parse<0>(&message[0]);
	xml_node<>* mesg = doc.first_node();
	string mesg_type = mesg->name();

	if (mesg_type == "logout")
	{
		logout(socket);
		return;
	}
	else if (mesg_type == "users")
		send_user_list(socket);
	else
		forward_message(socket, mesg, mesg_type, raw_mesg);
}

void Session::send_user_list(Socket& socket)
{
	string users_str = "<users>";
	for (auto it = socket_map.begin(); it != socket_map.end(); ++it)
		users_str += it->first + " ";
	users_str += "</users>\n";
	socket << users_str;

	return;
}

void Session::forward_message(Socket& socket, xml_node<>* mesg, string mesg_type, string raw_mesg)
{
	xml_node<>* recipient = mesg->first_node("recv");
	if (mesg_type == "mesg")
		socket << raw_mesg + "\n";
	while (recipient != NULL)
	{
		string recipient_id = recipient->value();
		socket_map[recipient_id] << raw_mesg + "\n";
		recipient = recipient->next_sibling("recv");
	}
}

bool Session::login(Socket& socket, Connection* db_con)
{
	string query1, query2;
	string input_password_hash, password_hash;
	query1 = "SELECT PASSWORD";
	query1 += "(" + curr_user.get_password() + ") AS input_pass";
	query2 = "SELECT Password from Users WHERE ID = ";
	query2 += "\'" + curr_user.get_id() + "\'";

	ResultSet* result1;
	ResultSet* result2;
	result1 = db_con->createStatement()->executeQuery(query1);
	result2 = db_con->createStatement()->executeQuery(query2);
	while (result1->next())
		input_password_hash = result1->getString("input_pass");
	while (result2->next())
		password_hash = result2->getString("Password");

	if (input_password_hash == password_hash)
	{
		cerr << "User " << curr_user.get_id() << " logged in successfully.\n";
		socket << "1\n";
		return true;
	}
	cerr << "User " << curr_user.get_id() << " failed to log in.\n";
	socket << "0\n";
	return false;
}

void Session::logout(Socket& socket)
{
	socket << "User logged out.\n";
	
	socket_map.erase(curr_user.get_id());

	return;
}
