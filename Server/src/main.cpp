#include "socket.hpp"
#include "server.hpp"
#include "global.hpp"

#include <cstdlib>
#include <cstring>
#include <cerrno>

#include <unordered_map>
#include <string>
#include <thread>
#include <stdexcept>

using namespace std;


unordered_map<string, Socket> socket_map;

int main(int argc, char* argv[])
{
	try
	{
		/* Create a server to handle each connection */
		Server server;
		/* Initialize and listen on the the socket */
		Socket socket(atoi(argv[1]));

		while (true)
		{
			try
			{
				Socket* connect_sock = new Socket;
				/* Accept a connection on the socket */
				socket.accept(*connect_sock);
				/* Pass control to server.connection_handler() */
				thread(&Server::connection_handler, server, ref(*connect_sock)).detach();
			}
			catch (runtime_error& e)
			{
				cerr << "# Runtime error exception: " << e.what() << "\n";
			}
		}
	}
	catch (runtime_error& e)
	{
		cerr << "# Runtime error exception: " << e.what() << "\n\n";
		exit(EXIT_FAILURE);
	}
	return 0;
}
