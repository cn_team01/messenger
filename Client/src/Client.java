import java.net.*;
import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

public class Client {
	public static Socket skt;
	public static String hostname; // = "140.112.41.99";
	public static int port; // = 5000;

	public static InputStream input_stream;
	public static OutputStream output_stream;
	public static BufferedReader in;

	public static JFrame frame;
	public static AuthPane authPane;
	public static MainPane mainPane;
	public static JLabel headerLabel;
	public static JLabel statusLabel;
	public static JPanel controlPanel;

	public static Status status;
	// public static ArrayList<String> receivers;

	public Client(String host, int port) {
		this.hostname = host;
		this.port = port;
		try {
			this.skt = new Socket(hostname, port);
			this.input_stream = skt.getInputStream();
			this.output_stream = skt.getOutputStream();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		this.in = new BufferedReader(new InputStreamReader(input_stream));
		this.frame = new JFrame("Messenger");
		frame.setSize(800,600);
		frame.setVisible(true);
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e){
				String logout = "<logout></logout>";
				try {
					output_stream.write(logout.getBytes());
					output_stream.flush();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				
				System.exit(0);
			}        
		});
	}

	public static void authenticate() {
		authPane = new AuthPane(output_stream);
		authPane.autoLogin("Snow_White", "12345678");
		JPanel contentPane = (JPanel) frame.getContentPane();
		contentPane.removeAll();
		contentPane.add(authPane);
		contentPane.revalidate(); 
		contentPane.repaint();
	}

	public static void messenger() {
		mainPane = new MainPane(output_stream, status);
		JPanel contentPane = (JPanel) frame.getContentPane();
		contentPane.removeAll();
		contentPane.add(mainPane);
		contentPane.revalidate(); 
		contentPane.repaint();

		String buf,root_name;
		Document doc;
		try {
			while(true) {		
				buf = in.readLine();
				System.out.println(buf);
				doc = loadXMLFromString(buf);
				root_name = doc.getDocumentElement().getNodeName();
				String ha="";

			//if mesg
				System.out.println("root_name: " + root_name);
				if(root_name.equals("mesg")){
					String sender = doc.getElementsByTagName("send").item(0).getTextContent();
					String receiver = "";
					String content;
					NodeList nList = doc.getElementsByTagName("recv");
					for(int i = 0; i < nList.getLength(); ++i){
						if (i == 0) {
							receiver += nList.item(i).getTextContent();
						} else {
							receiver += " " + nList.item(i).getTextContent();
						}
						
					}
					content = doc.getElementsByTagName("content").item(0).getTextContent();
					System.out.println(content);
					System.out.println("recv: " + receiver);
					int idx = mainPane.conv_list.is_exist(receiver + " " + sender);
					if (idx >= 0){
						String room = ConvList.conv_list.getModel().getElementAt(idx).
										replace(",", "").replace("[", "").replace("]", "");
						if (status.conv.equals(room)) {
							mainPane.dialog.refresh(sender, content);
						} else {
							status.conv = room;
							mainPane.dialog.init();
							mainPane.dialog.refresh(sender, content);
						}
						MainPane.room_name.setText("Chatroom: " + room);
						
					}
					else {
						mainPane.conv_list.add_conv(receiver + " " + sender);
						idx = ConvList.conv_list.getModel().getSize();
						MainPane.room_name.setText("Chatroom: " + ConvList.conv_list.getModel().getElementAt(idx-1).
										replace(",", "").replace("[", "").replace("]", ""));
						mainPane.dialog.init();
						mainPane.dialog.refresh(sender, content);
					}
					
				}
				//if
				else if(root_name.equals("file")){
					if(doc.getElementsByTagName("current").item(0).getTextContent().equals(1))
						ha="";
					ha += doc.getElementsByTagName("content").item(0).getTextContent();

					if(doc.getElementsByTagName("current").item(0).getTextContent().equals(
								doc.getElementsByTagName("length").item(0).getTextContent())){
						JFileChooser  fileDialog = new JFileChooser();
						int returnVal = fileDialog.showSaveDialog(frame);
						if (returnVal == JFileChooser.APPROVE_OPTION) {
							try{
								if (returnVal == JFileChooser.APPROVE_OPTION) {
									java.io.File file = fileDialog.getSelectedFile();
									FileOutputStream out = new FileOutputStream(file);
									byte[] data = ha.getBytes();
									out.write(data);
									out.close();
								}
								else{
									statusLabel.setText("Open command cancelled by user." );           
								}


							}


							catch(Exception fileE){
								fileE.printStackTrace();
							}
						}
					}

				}
				else if(root_name.equals("users")){
					String all_users = doc.getElementsByTagName("users").item(0).getTextContent();
					mainPane.userlist.refresh_user_list(all_users);
					
				}
				// lists
			}
		}
		catch(Exception e){
			return;
		}
	}

	public static Document loadXMLFromString(String xml) throws Exception
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		InputSource is = new InputSource(new StringReader(xml));
		return builder.parse(is);
	}

	public static void main(String[] args) {
		Client client = new Client(args[0], Integer.parseInt(args[1]));
		authenticate();
		try {
			while(!in.readLine().equals("1")) {
				System.out.println("Auth Failed");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Auth Succeed");
		status = new Status(output_stream, authPane.text_id.getText());
		System.out.println(status.send);
		messenger();
	}

}
