import java.net.*;
import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
class MainPane extends JPanel {
	OutputStream outputStream;
	public static Status status;
	public static JLabel room_name;
	static DialogPane dialog;
	InputText inputText;
	ButtonFile btn_file;
	UserList userlist;
	ConvList conv_list;

	public MainPane(OutputStream out, Status s) {
		this.outputStream = out;
		this.status = s;
		this.dialog = new DialogPane();
		this.inputText = new InputText(outputStream, status);
		this.btn_file = new ButtonFile(outputStream, status);
		this.conv_list = new ConvList(status);

		this.userlist= new UserList("",out);
		userlist.ask_server();

		this.setLayout(new GridBagLayout());
		this.room_name = new JLabel("ChatRoom: ");
		JLabel online = new JLabel("Online Users");

		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx=0.2;
		c.gridx = 0;
		c.gridy = 0;
		this.add(online,c);


		c.weightx = 0.8;
		c.gridx = 1;
		c.gridy = 0;
		this.add(room_name,c);

		c.fill = GridBagConstraints.VERTICAL;
		c.weightx=0.2;
		c.gridx = 0;
		c.gridy = 1;
		c.gridheight=2;
		this.add(userlist,c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 0.8;
		c.gridx = 1;
		c.gridy = 1;
		c.gridwidth = 2;
		c.gridheight = 1;
		this.add(dialog,c);


		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 2;
		c.gridwidth = 1;
		this.add(inputText,c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 2;
		c.gridy = 2;
		this.add(btn_file,c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 2;
		c.gridy = 1;
		this.add(conv_list);
	}
}
