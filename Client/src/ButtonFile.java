import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

class ButtonFile extends JButton {

	OutputStream outputStream;
	Status status;
	int BUFFERSIZE=1000;

	JFileChooser fc;
	FileInputStream fileInputStream = null;
	byte[] bFile;
	int returnVal;

	public ButtonFile(OutputStream out, Status s) {
		this.setText("Add a File");
		this.outputStream = out;
		this.status = s;
		this.fc = new JFileChooser();

		this.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				returnVal = fc.showOpenDialog(ButtonFile.this);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();
					try {
						bFile = new byte[(int) file.length()];
						fileInputStream = new FileInputStream(file);
						fileInputStream.read(bFile);
						fileInputStream.close();


						for (int i = 0; i < bFile.length; i++) {
							System.out.print((char)bFile[i]);
						}
						int size =0;

						int flag = 1;
						if(bFile.length%BUFFERSIZE == 0)
							flag = 0;
						for(int i = 0; i< bFile.length/BUFFERSIZE+flag ; ++i){
							byte[] contents = new byte[ 1024];
							byte[] buffer = new byte[ 1024];
							byte[] toServer = new byte[1024];

							String str = "<file><send>" + status.send + "</send>";
							for (int k = 0; k < status.recv.size(); k++) {
								str += "<recv>" + status.recv.get(k) + "</recv>";
							}
							str += "<filename>"+ fc.getName(file)+"</filename>";
							str += "<current>" + Integer.toString(i+1) + "</current>";
							str += "<length>" + Integer.toString(bFile.length/BUFFERSIZE+flag) + "</length>";
							str += "<content>";
							
							System.out.println(str);
							buffer = str.getBytes();
							int head_length = buffer.length;

							for(int j=i*BUFFERSIZE; j<(i+1)*BUFFERSIZE; ++j){
								System.out.println(j);
								if(j >= bFile.length)
									break;
								contents[j-i*BUFFERSIZE] = bFile[j];
							}

							for(int k=0; k<buffer.length + BUFFERSIZE; ++k){
								if(k >= bFile.length + head_length)
									break;
								if(k<buffer.length){
									toServer[k] = buffer[k];
								}
								else
									toServer[k] = contents[k-buffer.length];
								size = k;
							}
							buffer = "</content></file>".getBytes();
							for(int k=0; k<buffer.length; ++k)
								toServer[k+size] = buffer[k];
							System.out.println(17+size);
							Thread.sleep(50);
							outputStream.write(toServer);
							outputStream.flush();
						}

//						outputStream.write(str.getBytes());
//						outputStream.write(bFile);
//						outputStream.write(new String("</content></file>").getBytes());
						
					} catch(Exception ex){
						ex.printStackTrace();
					}				
				}
			}
		});
	}
}
