import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
class InputText extends JTextField {
	
	OutputStream outputStream;
	Status status;

	public InputText(OutputStream out, Status s) {
		this.outputStream = out;
		this.status = s;
		this.setPreferredSize(new Dimension(200, 24));

		this.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String str = ((InputText)e.getSource()).getText();
				String sendStr = "<mesg><send>" + status.send + "</send>";
				for(int i = 0; i < status.recv.size(); i++) {
					sendStr += "<recv>" + status.recv.get(i) + "</recv>";
				}
				sendStr += "<content>" + str + "</content></mesg>";
				try {
					outputStream.write(sendStr.getBytes());
					outputStream.flush();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});
	}
}