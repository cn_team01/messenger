import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class RoomList extends JScrollPane 
{
	DefaultComboBoxModel room_num;
	JComboBox room_combo;

	public RoomList(){
		super();
		System.out.println('a');
		room_num = new DefaultComboBoxModel();
		for(int i=0; i<20; ++i){
			room_num.addElement(Integer.toString(i+1));
		}
		room_combo = new JComboBox(room_num);
		room_combo.setSelectedIndex(0);
		room_combo.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				String data = "";
				if (room_combo.getSelectedIndex() != -1) {                     
					data = "Selected: " + room_combo.getItemAt(room_combo.getSelectedIndex());
					//TODO
				}            
			}

		});
		this.setViewportView(room_combo);
	}
}
