import java.net.*;
import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
class AuthPane extends JPanel {

	OutputStream outputStream;
	JLabel label_id;
	JLabel label_psd;
	JTextField text_id;
	JTextField text_psd;
	ButtonLogIn btn_login;
	ButtonSignUp btn_signup;

	public AuthPane(OutputStream out) {
		this.outputStream = out;
		label_id = new JLabel("User ID: ", JLabel.RIGHT);
		label_psd = new JLabel("Password: ", JLabel.RIGHT);
		text_id = new JTextField();
		text_psd = new JPasswordField();
		btn_login = new ButtonLogIn();
		btn_signup = new ButtonSignUp();

		text_id.setPreferredSize( new Dimension( 100, 24));
		text_psd.setPreferredSize( new Dimension( 100, 24));

		btn_login.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				String id = text_id.getText();
				String psd = text_psd.getText();
				String head = "<auth type=\"login\">"+"<id>" + id + "</id>" +
								"<pass>" + psd + "</pass>" + "</auth>";
				try {
					outputStream.write(head.getBytes());
					outputStream.flush();
				} catch (Exception ex) {
					ex.printStackTrace();
				}	
			}
		});

		btn_signup.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				String id = text_id.getText();
				String psd = text_psd.getText();
				String head = "<auth type=\"signup\">"+"<id>" + id + "</id>" +
								"<pass>" + psd + "</pass>" + "</auth>";
				try {
					outputStream.write(head.getBytes());
					outputStream.flush();
				} catch (Exception ex) {
					ex.printStackTrace();
				}		
			}
		});

		this.add(label_id);
		this.add(text_id);
		this.add(label_psd);
		this.add(text_psd);
		this.add(btn_login);
		this.add(btn_signup);
	}

	public void autoLogin(String id, String psd) {
		text_id.setText(id);
		text_psd.setText(psd);
	}
}