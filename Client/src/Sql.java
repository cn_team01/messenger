import java.sql.*;
import java.util.*;

public class Sql {

	private Connection connection;
	private Statement statement;

	/**
	 * Sql Constructor: create a database named "messenger"
	 */
	public Sql() {
		try {
			connection = DriverManager.getConnection("jdbc:sqlite:messenger.db");
			statement = connection.createStatement();
			statement.setQueryTimeout(30);
			statement.executeUpdate("create table if not exists friends (id string, primary key(id))");
			statement.executeUpdate("create table if not exists conversations (name string, members string)");
		}
		catch (SQLException e) {
			System.out.print("ERROR[init]: ");
			System.err.println(e.getMessage());
		}
	}

	/**
	 * Sql Constructor: create a database with given name
	 * @param  db_name the name of the db file
	 */
	public Sql(String db_name) {
		try {
			connection = DriverManager.getConnection("jdbc:sqlite:" + db_name + ".db");
			statement = connection.createStatement();
			statement.setQueryTimeout(30);
			statement.executeUpdate("create table if not exists friends (id string, primary key(id))");
			statement.executeUpdate("create table if not exists conversations (name string, members string)");
		}
		catch (SQLException e) {
			System.out.print("ERROR[init]: ");
			System.err.println(e.getMessage());
		}
	}

	/**
	 * add a new conversation to db
	 * @param conv conversation name
	 */
	public void new_conv(String conv) {
		try {
			statement.executeUpdate("create table " + conv + " (id string, msg string)");
		}
		catch (SQLException e) {
			System.out.print("ERROR[new_conv]: ");
			System.err.println(e.getMessage());
		}
		return;
	}

	/**
	 * add a new message to db
	 * @param conv conversation name
	 * @param id   the person's id that send the message
	 * @param msg  message string
	 */
	public void new_mesg(String conv, String id, String msg) {
		try {
			statement.executeUpdate("insert into " + conv + " values('" + id + "', '" + msg + "')");
		}
		catch (SQLException e) {
			System.out.print("ERROR[new_mesg]: ");
			System.err.println(e.getMessage());
		}
		return;
	}

	/**
	 * get all the old messages in a conversation
	 * @param  conv conversation name
	 * @return      an ArrayList of messages in the form of "ID: MESG"
	 */
	public ArrayList<String> query_history(String conv) {
		ArrayList<String> hist = new ArrayList<String>();
		try {
			ResultSet rs = statement.executeQuery("select * from " + conv);
			while (rs.next()) {
				hist.add(rs.getString("id") + ": " + rs.getString("msg"));
			}
		}
		catch (SQLException e) {
			System.out.print("ERROR[query_history]: ");
			System.err.println(e.getMessage());
		}
		return(hist);
	}

	/**
	 * get friend list
	 * @return an string ArrayList of IDs
	 */
	public ArrayList<String> query_all_friends() {
		ArrayList<String> list = new ArrayList<String>();
		try {
			ResultSet rs = statement.executeQuery("select * from friends");
			while (rs.next()) {
				list.add(rs.getString("id"));
			}
		}
		catch (SQLException e) {
			System.out.print("ERROR[query_all_friends]: ");
			System.err.println(e.getMessage());
		}
		return(list);
	}

	/**
	 * add a new friend to database
	 * @param id friend's id
	 */
	public void new_friend(String id) {
		try {
			statement.executeUpdate("insert into friends values('" + id + "')");
		}
		catch (SQLException e) {
			System.out.print("ERROR[new_friend]: ");
			System.err.println(e.getMessage());
		}
		return;
	}

}