import java.io.*;
import java.util.*;
class Status {
	OutputStream outputStream;
	String send;
	String conv;
	ArrayList<String> recv;

	public Status(OutputStream out, String send) {
		this.outputStream = out;
		this.send = send;
		this.conv = "";
		this.recv = new ArrayList<String>();
	}

	public void setSender(String send) {
		this.send = send;
	}

	public void setReceiver(ArrayList<String> r){
		System.out.println("setRecv: " + r.toString());
		this.recv.clear();
		for(int i = 0; i < r.size(); i++) {
			this.recv.add(r.get(i));
		}
	}

}
