import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
class DialogPane extends JScrollPane {
	JTextArea dialog;
	public DialogPane() {
		super(VERTICAL_SCROLLBAR_AS_NEEDED, HORIZONTAL_SCROLLBAR_AS_NEEDED);
		this.setPreferredSize( new Dimension(400, 300));
		dialog = new JTextArea(400, 300);
		dialog.setMargin(new Insets(5, 5, 5, 5));
		dialog.setEditable(false);
		this.setViewportView(dialog);
	}

	public void refresh(String sender, String mesg) {
		dialog.append(sender + ": " + mesg + "\n");
		dialog.setCaretPosition(dialog.getDocument().getLength());
	}

	public void init() {
		dialog.setText("");
		dialog.setCaretPosition(dialog.getDocument().getLength());
	}
}