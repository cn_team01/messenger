import java.net.*;
import java.io.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
class ConvList extends JPanel {
	Status status;
	static JList<String> conv_list;
	DefaultListModel<String> listmodel;
	ArrayList<String> users;
	static ArrayList<ArrayList<String>> convs;
	JScrollPane scroll_pane;

	public ConvList(Status s) {
		this.status = s;
		this.setPreferredSize(new Dimension(300, 100));
		this.listmodel = new DefaultListModel<String>();
		this.conv_list = new JList<String>(this.listmodel);
		this.conv_list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.scroll_pane = new JScrollPane(this.conv_list);
		scroll_pane.setPreferredSize(new Dimension(300, 200));
		this.convs = new ArrayList<ArrayList<String>>();
		this.add(scroll_pane);

		this.conv_list.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				String selected_conv = (String)((JList<String>)e.getSource()).getSelectedValue();
				int selected_idx = ((JList<String>)e.getSource()).getSelectedIndex();
				if(!status.recv.toString().equals(convs.get(selected_idx).toString())) {
					MainPane.dialog.init();
					status.setReceiver(convs.get(selected_idx));
					MainPane.room_name.setText("Chatroom: " + selected_conv.replace(",", "").replace("[", "").replace("]", ""));
				}
			}
		});
	}

	public void add_conv(String chatroom) {
		String[] users = chatroom.split(" ");
		ArrayList<String> tmp_list = new ArrayList<String>();

		for(int i = 0; i < users.length; i++) {
			if (!users[i].equals(status.send)){
				tmp_list.add(users[i]);
			}
		}
		Collections.sort(tmp_list);
		this.convs.add(tmp_list);
		this.listmodel.addElement(tmp_list.toString());
		status.setReceiver(tmp_list);
		status.conv = tmp_list.toString().replace(",", "").replace("[", "").replace("]", "");
		this.validate();
		this.repaint();
	}

	public int is_exist(String chatroom) {
		String[] users = chatroom.split(" ");
		ArrayList<String> tmp_list = new ArrayList<String>();
		for(int i = 0; i < users.length; i++) {
			if (!users[i].equals(status.send)){
				tmp_list.add(users[i]);
			}
		}
		Collections.sort(tmp_list);

		for (int i = 0; i < this.convs.size(); i++) {
			if(conv_list.getModel().getElementAt(i).equals(tmp_list.toString())){
				return i;
			}
		}
		return -1;
	}


}