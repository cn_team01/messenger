import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

class UserList extends JPanel
{
	DefaultListModel user_name;
	JList user_list;
	JScrollPane user_scroll_list;
	JButton refresh;//refresh button
	JButton chat;//chat button: open new chatroom window
	OutputStream out;
	public UserList(String users, OutputStream outToServer){
		super();
		out = outToServer;

		//get strings of users(e.g "snow_white hahaman yooyooball")
		String [] parts = users.split(" ");
		user_name = new DefaultListModel();

		//add each user
		for(int i = 0; i< parts.length; ++i){
			user_name.addElement(parts[i]);
		}

		user_list = new JList(user_name);
		user_list.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		//put it into scrollpane
		user_scroll_list  =  new JScrollPane(user_list,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		this.setLayout(new BorderLayout());
		this.add(user_scroll_list);

		JPanel buttons = new JPanel(new FlowLayout());
		refresh = new JButton("Refresh");
		chat = new JButton("chat");

		//send user list request
		refresh.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				try{
					String tmp = "<users></users>";
					out.write(tmp.getBytes());
					out.flush();
				}
				catch(Exception ee){
					ee.printStackTrace();
				}
			}
		});

		//open new chat
		chat.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				String tmp="";
				ArrayList<String> recv = new ArrayList<String>();
				
				if(user_list.getSelectedIndex() != -1) {
					for(Object user:user_list.getSelectedValues()){
						tmp += user + " ";
						recv.add(user+"");
					}
					MainPane.status.setReceiver(recv);
					MainPane.room_name.setText("ChatRoom: " + tmp);
				//	JFrame mainFrame = new JFrame(tmp);
				//	mainFrame.addWindowListener(new WindowAdapter() {
				//		public void windowClosing(WindowEvent windowEvent){
				//			System.exit(0);
				//		}        
				//	}); 
				//	mainFrame.setSize(600,400);
				//	mainFrame.setVisible(true);
				}
			}

		});

		buttons.add(refresh);
		buttons.add(chat);
		this.add(buttons,BorderLayout.PAGE_END);
	}

	//replace the current list to users string(user string e.g:"asdf ddd you snow_white")
	public void refresh_user_list(String users){
		user_name.removeAllElements();
		String [] parts = users.split(" ");
		//add each user
		for(int i = 0; i< parts.length; ++i){
			if(!parts[i].equals(MainPane.status.send)){
				user_name.addElement(parts[i]);
			}
		}
	}

	//request user list
	public void ask_server(){
		try{
			String tmp = "<users></users>";
			out.write(tmp.getBytes());
			out.flush();
		}
		catch(Exception ee){
			ee.printStackTrace();
		}
	}

}
