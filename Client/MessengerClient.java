import java.net.*;
import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class MessengerClient
{

	public static Socket client;//client socket
	public static int port;//server port number
	public static String serverName;//server port name
	public static String user_id;// user id

	//gui
	public static JFrame mainFrame;
	public static JLabel headerLabel;
	public static JLabel statusLabel;
	public static JPanel controlPanel;


	public static void main(String [] args) throws InterruptedException
	{
		//java MessengerClient serverName port
		serverName = args[0];
		port = Integer.parseInt(args[1]);

		try
		{
			System.out.println("Welcome!");
			System.out.println("Connecting to " + serverName + " on port " + port);

			//connect to server Socket
			client = new Socket(serverName, port);
			System.out.println("Just connected to " + client.getRemoteSocketAddress());

			prepareGUI();
			init_GUI();

			Thread t = new Thread(new ReadServer());
			t.start();
			
			//close client
			//client.close();

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void prepareGUI()
	{
		//window title name
		mainFrame = new JFrame("HeyBro!");
		//window size
		mainFrame.setSize(800,450);
		// layout
		mainFrame.setLayout(new GridLayout(3, 1));
		headerLabel = new JLabel("",JLabel.CENTER );
		mainFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowEvent){
				System.exit(0);
			}        
		}); 
		headerLabel = new JLabel("", JLabel.CENTER);        
		statusLabel = new JLabel("",JLabel.CENTER);

		statusLabel.setSize(450,100);

		controlPanel = new JPanel();
		controlPanel.setLayout(new FlowLayout());

		mainFrame.add(headerLabel);
		mainFrame.add(controlPanel);
		mainFrame.add(statusLabel);
		mainFrame.setVisible(true);  
	}

	public static void init_GUI()
	{
		headerLabel.setText("Thanks for using HeyBro! Please sign up or login");

		JLabel  user_id_label = new JLabel("User ID: ", JLabel.RIGHT);
		JTextField user_id_text = new  JTextField();
		JLabel user_passwd_label = new JLabel("password: ", JLabel.RIGHT);
		JTextField user_passwd_text = new JPasswordField();

		JButton SignupButton = new JButton("Sign up");
		JButton LoginButton = new JButton("Login");

		GridLayout layout = new GridLayout(0,2);
		layout.setHgap(10);
		layout.setVgap(10);

		JPanel panel = new JPanel();
		panel.setLayout(layout);
		panel.add(user_id_label);
		panel.add(user_id_text);
		panel.add(user_passwd_label);
		panel.add(user_passwd_text);
		panel.add(SignupButton);
		panel.add(LoginButton);

		controlPanel.add(panel);

		//add singup action
		SignupButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				try{
					OutputStream outToServer = client.getOutputStream();
					String tmp = "<auth type=\"signup\">"+"<id>"+user_id_text.getText()+"</id>"+
						"<pass>"+user_passwd_text.getText()+"</pass>"+"</auth>";
					outToServer.write(tmp.getBytes());
					outToServer.flush();

					InputStream inFromServer = client.getInputStream();
					InputStreamReader inFS = new InputStreamReader(inFromServer);
					BufferedReader in = new BufferedReader(inFS);

					statusLabel.setText(in.readLine());
				}
				catch(Exception ee){
					ee.printStackTrace();
				}
			}
		});

		//add login action
		LoginButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				try{
					OutputStream outToServer = client.getOutputStream();
					String tmp = "<auth type=\"login\">"+"<id>"+user_id_text.getText()+"</id>"+
						"<pass>"+user_passwd_text.getText()+"</pass>"+"</auth>";
					//debug//
					System.out.println(tmp);
					//debug//

					outToServer.write(tmp.getBytes());
					outToServer.flush();

					InputStream inFromServer = client.getInputStream();
					InputStreamReader inFS = new InputStreamReader(inFromServer);
					BufferedReader in = new BufferedReader(inFS);

					statusLabel.setText(in.readLine());
					//init_chatroomGUI();
				}
				catch(Exception ee){
					ee.printStackTrace();
				}
			}

		});

		mainFrame.setVisible(true);
	}
	/*
	public static void init_chatroomGUI(){
		//window size
	//	mainFrame.removeALL();
		mainFrame.setSize(1600,900);

		JPanel chatPane = new JPanel(new BorderLayout());
		JTextArea chatText = new JTextArea(10, 20);
		chatText.setLineWrap(true);
		chatText.setEditable(false);
		chatText.setForeground(Color.blue);
		JScrollPane chatTextPane = new JScrollPane(chatText,
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		JTextField chatLine = new JTextField();
		chatLine.setEnabled(false);
		chatPane.add(chatLine, BorderLayout.SOUTH);
		chatPane.add(chatTextPane, BorderLayout.CENTER);
		chatPane.setPreferredSize(new Dimension(200, 200));

		mainFrame.setVisible(true);
	}*/
}
