import java.net.*;
import java.io.*;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

public class ReadServer implements Runnable
{
	public void run() {
		try{
			OutputStream outToServer = MessengerClient.client.getOutputStream();
			InputStream inFromServer = MessengerClient.client.getInputStream();
			InputStreamReader inFS = new InputStreamReader(inFromServer);
			BufferedReader in = new BufferedReader(inFS);

			String buf,root_name;

			Document doc;

			while(true){
				buf = in.readLine();
				doc = loadXMLFromString(buf);
				root_name = doc.getDocumentElement().getNodeName();

				//if mesg
				if(root_name.equals("mesg")){
					String sender = doc.getElementsByTagName("send").item(0).getTextContent();
					String receiver = "";
					String content;
					NodeList nList = doc.getElementsByTagName("recv");
					for(int i = 0; i < nList.getLength(); ++i){
						receiver += nList.item(i).getTextContent();
					}
					content = doc.getElementsByTagName("content").item(0).getTextContent();
					//TODO

				}
				//if
				else if(root_name.equals("file")){
					//TODO
				}
				else if(root_name.equals("knock")){
					//TODO
				}
				
			}


		}
		catch(Exception e){
			return;
		}


	}

	public static Document loadXMLFromString(String xml) throws Exception
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		InputSource is = new InputSource(new StringReader(xml));
		return builder.parse(is);
	}

}

