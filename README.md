# A Messenger Implemented in JAVA

### [Hackpad](https://hackpad.com/Messenger-andSgNmBmq7)

### [System Structure](https://cacoo.com/diagrams/p87yYVIEleANokGq/edit)

## References:

1. [Gradle Tutorial](https://docs.gradle.org/current/userguide/tutorials.html)

2. [Java - Networking](http://www.tutorialspoint.com/java/java_networking.htm)

3. [Java - Multithreading](http://www.tutorialspoint.com/java/java_multithreading.htm)

4. [Java Examples - Multithreaded Server](http://www.tutorialspoint.com/javaexamples/net_multisoc.htm)

5. [Java Web Start](https://docs.oracle.com/javase/tutorial/deployment/webstart/index.html)

6. [An Introduction to Java Database (JDBC) Programming](https://www.ntu.edu.sg/home/ehchua/programming/java/JDBC_Basic.html)

7. [Protocol Design](http://www.egr.msu.edu/classes/ece480/capstone/fall12/group02/documents/Ryan-Lattrel_App-Note.pdf)

8. [Protocol Design-2](http://www.cs.rpi.edu/courses/fall02/netprog/notes/protocols/protocols.pdf)
